# Ukraine.sol

## function

### ERC1155 function

- For ERC1155 default function please check [Openzeppelin doc](https://docs.openzeppelin.com/contracts/4.x/api/token/erc1155)

### transaction function

#### userMint

```function userMint (address adr, uint id, uint256 amount, bytes memory data)```

- User can mint additional token which is existed with token price

- payable function

- parameters:

|name|type|description|
|:--|:--|:----|
|adr|address|鑄造地址|
|id|uint|tokenId|
|amount|uint|數量|
|data|bytes|夾帶資訊, 如不須可帶 0x00,不會影響鑄造功能|

- event:

  - TransferSingle(operator, from, to, id, amount)

    - parameters:

    |name|type|description|
    |:--|:--|:----|
    |operator|address|操作者地址|
    |from|address|發送者地址|
    |to|address|接收者地址|
    |id|uint|tokenId|
    |amount|uint|數量|

  - MintToken(tokenId, owner, amount, price, operator);

    - parameters:

    |name|type|description|
    |:--|:--|:----|
    |tokenId|uint|tokenId|
    |owner|address|接收者地址|
    |amount|uint|數量|
    |price|uint|token 價格|
    |operator|address|操作者地址|

#### ownerMint

```function ownerMint (address adr, uint id, uint256 amount, uint mintPrice, bytes memory data)```

- Owner can mint token , if the token existed just add token supply, else mint a new token and set the token price

- Only contract owner can execute this function

- parameters:

|name|type|description|
|:--|:--|:----|
|adr|address|鑄造地址|
|id|uint|tokenId|
|amount|uint|數量|
|mintPrice|uint|鑄造價格|
|data|bytes|夾帶資訊, 如不須可帶 0x00,不會影響鑄造功能|

- event:

  - TransferSingle(operator, from, to, id, amount)

    - parameters:

    |name|type|description|
    |:--|:--|:----|
    |operator|address|操作者地址|
    |from|address|發送者地址|
    |to|address|接收者地址|
    |id|uint|tokenId|
    |amount|uint|數量|

  - NewToken(tokenId, owner, amount, price, operator);

    - parameters:

    |name|type|description|
    |:--|:--|:----|
    |tokenId|uint|tokenId|
    |owner|address|接收者地址|
    |amount|uint|數量|
    |price|uint|token 價格|
    |operator|address|操作者地址|

#### setURI

```function setURI(string memory uri_)```

- Owner can change the token uri

- Only contract owner can execute this function

- parameters:

|name|type|description|
|:--|:--|:----|
|uri_|string|token uri|

- Events

  - SetURI(uri)

    - parameters:

    |name|type|description|
    |:--|:--|:----|
    |uri|string|token uri|

#### addMaxLimit

```function addMaxLimit(uint256 tokenId, uint amount)```

- Owner can set the token max supply

- Only contract owner can execute this function

- parameters:

|name|type|description|
|:--|:--|:----|
|tokenId|uint|token id|
|amount|uint|欲增加上限數量|

- Events

  - AddTokenMaxLimit

    - parameters:

    |name|type|description|
    |:--|:--|:----|
    |tokenId|uint|token id|
    |amount|uint|欲增加上限數量|

#### setVault

```function setVault(address adr)```

- Owner can set the vault address

- Only contract owner can execute this function

- parameters:

|name|type|description|
|:--|:--|:----|
|adr|address|vault地址|

- Events

  - VaultChange

    - parameters:

    |name|type|description|
    |:--|:--|:----|
    |operator|address|操作者地址|
    |vault|address|vault 合約地址|

#### setPrice

```setPrice(uint256 tokenId, uint256 mintPrice)```

- Owner can set the token price

- Only contract owner can execute this function

- parameters:

|name|type|description|
|:--|:--|:----|
|tokenId|uint|token id|
|mintPrice|uint|鑄造價格|

- Events

  - PriceChange

    - parameters:

    |name|type|description|
    |:--|:--|:----|
    |operator|address|操作者地址|
    |tokenId|uint|token id|
    |price|uint|價格|

### view function

#### uri

```function uri(uint256 _tokenid)```

- get the token uri

- parameters:

|name|type|description|
|:--|:--|:----|
|_tokenid|uint|token id|

- return:

```http://localhost/test/1.json```